// ---------------------
// TestAllocator.c++
// @author Grant Fischer
// Date: 03/30/2020
// ---------------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

using namespace testing;

// -----------------
// AllocatorFixture1
// -----------------

template <typename T>
struct AllocatorFixture1 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_1 =
    Types<
    std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1);

TYPED_TEST(AllocatorFixture1, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type x;                           // T x
    const size_type  s = 1;                     // T::size_type
    const value_type v = 2;                     // T::value_type
    const pointer    p = x.allocate(s);         // T::pointer
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture1, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// -----------------
// AllocatorFixture2
// -----------------

template <typename T>
struct AllocatorFixture2 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_2 =
    Types<
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2);

TYPED_TEST(AllocatorFixture2, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture2, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TYPED_TEST(AllocatorFixture2, test2) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type x;                           // T x
    const size_type  s = 7;                     // T::size_type
    const value_type v = 99;                    // T::value_type
    pointer          p = x.allocate(s);         // T::pointer
    if (p != nullptr) {
        x.construct(p, v);
        x.deallocate(p, s);
        p = x.allocate(s);
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture2, test3) {
    using allocator_type = typename TestFixture::allocator_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, 1111);
        x.construct(p, 2222);
        x.construct(p, 3333);
        x.construct(p, 4444);
        x.construct(p, 5555);
        x.construct(p, 1);
        ASSERT_EQ(*p, 1);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture2, test4) {
    my_allocator<size_t, 1000> x;
    size_t* p = x.allocate(10);
    x.deallocate(p, 1);
    my_allocator<size_t, 1000>::iterator b = x.begin();
    my_allocator<size_t, 1000>::iterator e = x.end();
    ASSERT_EQ(b, e);
}

TYPED_TEST(AllocatorFixture2, test5) {
    my_allocator<size_t, 1000> x;
    size_t* p = x.allocate(10);
    my_allocator<size_t, 1000>::iterator b = x.begin();
    my_allocator<size_t, 1000>::iterator e = x.end();
    ++b;
    ASSERT_EQ(b, e);
    x.deallocate(p, 1);
}

TYPED_TEST(AllocatorFixture2, test6) {
    my_allocator<size_t, 1000> x;
    size_t* p1 = x.allocate(5);
    size_t* p2 = x.allocate(10);
    x.construct(p1, 99);
    x.construct(p2, 88);
    
    my_allocator<size_t, 1000>::iterator b = x.begin();
    ++b;
    int* ptr = &*b;
    ++ptr;
    ASSERT_EQ(*p2, *ptr);
    x.destroy(p1);
    x.destroy(p2);
    x.deallocate(p1, 1);
    x.deallocate(p2, 1);
}

TYPED_TEST(AllocatorFixture2, test7) {
    my_allocator<size_t, 1000> x;
    size_t* p = x.allocate(124);
    my_allocator<size_t, 1000>::iterator b = x.begin();
    my_allocator<size_t, 1000>::iterator e = x.end();
    ASSERT_EQ(b, e);
    x.deallocate(p, 1);
}
