// ---------------------
// Allocator.h
// @author Grant Fischer
// Date: 03/30/2020
// ---------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept>

#include <sstream>

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     *
     * Checks that the array is filled with valid blocks and no two consecutive blocks are
     * free blocks. A valid block has matching sentinels that are != 0
     * @return true if the blocks in the array are in a valid configuration, false otherwise
     */
    bool valid () const {
        // check if currrent free block directly follows a free block
        bool prevFree = 0;
        bool currFree = 0;
        const_iterator b1 = begin();
        const_iterator e1 = end();

        while (b1 != e1) {
            currFree = *b1 > 0;
            if (prevFree && currFree)
                return false;
            prevFree = currFree;
            ++b1;
        }

        // declare variables and check validity of last block
        const_iterator b2 = begin();
        const_iterator e2 = end();

        int* startSent = const_cast<int*>(&*e2);
        int startSentVal = *startSent;
        ++startSent;
        char* tempSent = reinterpret_cast<char*>(startSent);
        tempSent += abs(startSentVal);
        int* endSent = reinterpret_cast<int*>(tempSent);
        int endSentVal = *endSent;

        if (startSentVal != endSentVal)
            return false;

        // ensure rest of blocks are valid
        while (b2 != e2) {
            startSent = const_cast<int*>(&*e2);
            startSentVal = *startSent;
            ++startSent;
            tempSent = reinterpret_cast<char*>(startSent);
            tempSent += abs(startSentVal);
            endSent = reinterpret_cast<int*>(tempSent);
            endSentVal = *endSent;

            if (startSentVal != endSentVal)
                return false;
            ++b2;}

        return true;
    }

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            // <your code>
            return (*lhs == *rhs);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            // <your code>
            return *_p;
        }

        // -------------
        // operator ++
        // pre increment
        // -------------

        iterator& operator ++ () {
            // <your code>
            int* iptr = _p;
            int sentVal = abs(*iptr);
            char* cptr = reinterpret_cast<char*>(iptr);
            cptr = cptr + sentVal + (2 * sizeof(int));
            int* newiptr = reinterpret_cast<int*>(cptr);
            _p = newiptr;
            return *this;
        }

        // --------------
        // operator ++
        // post increment
        // --------------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -------------
        // operator --
        // pre decrement
        // -------------

        iterator& operator -- () {
            // <your code>
            int* iptr = _p;
            --iptr;
            int sentVal = abs(*iptr);
            char* cptr = reinterpret_cast<char*>(iptr);
            cptr = cptr - sentVal - sizeof(int);
            int* newiptr = reinterpret_cast<int*>(cptr);
            _p = newiptr;
            return *this;
        }

        // --------------
        // operator --
        // post decrement
        // --------------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            // <your code>
            return (*lhs == *rhs);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            const int& ci = *_p;
            return ci;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            // <your code>
            int* iptr = _p;
            int sentVal = abs(*iptr);
            ++iptr;
            char* cptr = reinterpret_cast<char*>(iptr);
            cptr += sentVal;
            iptr = reinterpret_cast<int*>(cptr);
            ++iptr;
            _p = iptr;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            // <your code>
            int* iptr = _p;
            --iptr;
            int sentVal = abs(*iptr);
            char* cptr = reinterpret_cast<char*>(iptr);
            cptr = cptr - sentVal - sizeof(int);
            int* newiptr = reinterpret_cast<int*>(cptr);
            _p = newiptr;
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // <your code>
        if (N < sizeof(T) + (2 * sizeof(int)))
            throw std::bad_alloc();

        int initSent = N - (2 * sizeof(int));

        iterator b = begin();
        *b = initSent;

        // (*this)[0] = initSent;
        // (*this)[N - 1] = initSent;

        // Create initial sentinels in the array
        int* sentinel = reinterpret_cast<int*>(a);
        // *sentinel = initSent;
        ++sentinel;
        char* temp = reinterpret_cast<char*>(sentinel);
        temp = temp + initSent;
        sentinel = reinterpret_cast<int*>(temp);
        *sentinel = initSent;

        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if s is invalid
     *
     * Allocates space for s T's in the array using first-fit,
     * If the remaining space is less than a block, include the remaining space in the
     * allocation and update the sentinels accordingly
     * @param s the number of T objects to allocate space for
     * @return T* of the start of the data chuck of the allocated block
     */
    pointer allocate (size_type s) {
        assert(s >= 1);

        // iterators
        iterator b = begin();
        iterator e = end();

        // declare variables
        pointer data_ptr;
        int* block_ptr = 0;
        int minBlockSize = sizeof(T) + (2 * sizeof(int));
        int bytesNeeded = (s * sizeof(T)) + (2 * sizeof(int));
        int sentinelNeeded = (s * sizeof(T));
        int sentVal = *b;

        // find block to use
        while (b != e) {
            sentVal = *b;
            if (sentVal >= sentinelNeeded)
                break;
            ++b;
        }
        if (sentVal < sentinelNeeded) {
            // b == e and still haven't found space
            if(*e < sentinelNeeded || *e < 0)
                throw std::bad_alloc();
            sentVal = *e;
            block_ptr = &*e;
        }
        else {
            block_ptr = &*b;
        }

        // check left over bytes to see if need to include in allocation
        int remainingBytes = sentVal - sentinelNeeded;
        if (remainingBytes < minBlockSize) {
            // Case 1: not enough space leftover, just allocate whole block
            *block_ptr = *block_ptr * -1;
            ++block_ptr;
            data_ptr = reinterpret_cast<pointer>(block_ptr);
            char* char_ptr = reinterpret_cast<char*>(block_ptr);
            char_ptr += sentVal;
            int* endSent = reinterpret_cast<int*>(char_ptr);
            *endSent = *endSent * -1;
        }
        else {
            // Case 2: need to allocate exact amount, then create a free block following

            // Used Block
            int usedSentVal = bytesNeeded - (2 * sizeof(int));
            *block_ptr = (usedSentVal * -1);
            ++block_ptr;
            data_ptr = reinterpret_cast<pointer>(block_ptr);
            char* char_ptr = reinterpret_cast<char*>(block_ptr);
            char_ptr += usedSentVal;
            int* tempSent = reinterpret_cast<int*>(char_ptr);
            *tempSent = (usedSentVal * -1);

            // Free Block
            int newSentVal = sentVal - bytesNeeded;
            ++tempSent;
            *tempSent = newSentVal;
            ++tempSent;
            char* cptr = reinterpret_cast<char*>(tempSent);
            cptr += newSentVal;
            int* endSent = reinterpret_cast<int*>(cptr);
            *endSent = newSentVal;
        }

        assert(valid());

        return data_ptr;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     *
     * Invoke's T's value constructor in the space in the array at pointer p
     * @param p the pointer at which to construct the T object
     * @param v the value of the T object in which to construct
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    /*
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     *
     * Deallocates the block of memory containing pointer p
     * @param p the pointer of the beginning of the data chunk of the block to deallocate
     */
    void deallocate (pointer p, size_type) {
        // check that the pointer is valid
        iterator b = begin();
        iterator e = end();

        int* bptr = &*b;
        ++bptr;
        if ((void*)p < (void*)bptr)
            throw std::invalid_argument("Pointer to deallocate is before first block");
        int* eptr = &*e;
        ++eptr;
        if ((void*)p > (void*)eptr)
            throw std::invalid_argument("Pointer to deallocate is after last block");
        int* iptr = reinterpret_cast<int*>(p);
        int* saveptr = iptr;
        --iptr;
        int headerSentinelVal = *iptr;
        if (headerSentinelVal >= 0)
            throw std::invalid_argument("Trying to deallocate a free block");

        // flip sentinels' signs
        headerSentinelVal = abs(headerSentinelVal);
        *iptr = headerSentinelVal; //left
        ++iptr;
        char* cptr = reinterpret_cast<char*>(iptr);
        cptr = cptr + headerSentinelVal;
        iptr = reinterpret_cast<int*>(cptr);
        *iptr = headerSentinelVal; //right

        // check which adjacent free blocks if any to coalesce with
        iterator b2 = begin();
        iterator e2 = end();
        bool coalesceRight = 0;
        bool coalesceLeft = 0;
        int* minBlockAddr = &*b2;
        int* maxBlockAddr = &*e2;
        if (iptr > maxBlockAddr) {
            coalesceRight = 0;
        }
        else {
            ++iptr;
            if (*iptr > 0)
                coalesceRight = 1;
        }
        --iptr;
        cptr = reinterpret_cast<char*>(iptr);
        cptr -= headerSentinelVal;
        iptr = reinterpret_cast<int*>(cptr);
        --iptr;
        if (iptr == minBlockAddr) {
            coalesceLeft = 0;
        }
        else {
            --iptr;
            if (*iptr > 0)
                coalesceLeft = 1;
        }

        // coalesce - (yeah ik it would have been so much easier
        //      using iterators, I was just too far deep, I felt dumb when I realized)
        int* intPtr = saveptr;
        char* charPtr = 0;
        int temp = 0;
        int sentinelVal = 0;
        if (coalesceLeft && coalesceRight) {
            sentinelVal = 4 * sizeof(int);
            --intPtr;
            temp = *intPtr;
            sentinelVal += temp;
            --intPtr;
            temp = *intPtr;
            sentinelVal += temp;
            ++intPtr;
            temp = *intPtr;
            ++intPtr;
            charPtr = reinterpret_cast<char*>(intPtr);
            charPtr += temp;
            intPtr = reinterpret_cast<int*>(charPtr);
            ++intPtr;
            temp = *intPtr;
            sentinelVal += temp;
            ++intPtr;
            charPtr = reinterpret_cast<char*>(intPtr);
            charPtr += temp;
            intPtr = reinterpret_cast<int*>(charPtr);
            *intPtr = sentinelVal;                     // write right sentinel
            charPtr = reinterpret_cast<char*>(intPtr);
            charPtr -= sentinelVal;
            intPtr = reinterpret_cast<int*>(charPtr);
            --intPtr;
            *intPtr = sentinelVal;                    // write left sentinel
        }
        else if (coalesceLeft && !coalesceRight) {
            sentinelVal = 2 * sizeof(int);
            --intPtr;
            temp = *intPtr;
            sentinelVal += temp;
            --intPtr;
            temp = *intPtr;
            sentinelVal += temp;
            charPtr = reinterpret_cast<char*>(intPtr);
            charPtr -= temp;
            intPtr = reinterpret_cast<int*>(charPtr);
            --intPtr;
            *intPtr = sentinelVal;                     // write left sentinel
            ++intPtr;
            charPtr = reinterpret_cast<char*>(intPtr);
            charPtr += sentinelVal;
            intPtr = reinterpret_cast<int*>(charPtr);
            *intPtr = sentinelVal;                        // write right sentinel
        }
        else if (!coalesceLeft && coalesceRight) {
            sentinelVal = 2 * sizeof(int);
            --intPtr; //intPtr == minBlockAddr
            temp = *intPtr;
            sentinelVal += temp;
            ++intPtr;
            charPtr = reinterpret_cast<char*>(intPtr);
            charPtr += temp;
            intPtr = reinterpret_cast<int*>(charPtr);
            ++intPtr; //now on start of right block
            temp = *intPtr;
            ++intPtr;
            sentinelVal += temp;
            charPtr = reinterpret_cast<char*>(intPtr);
            charPtr += temp;
            intPtr = reinterpret_cast<int*>(charPtr);
            *intPtr = sentinelVal;                         // write right sentinel
            charPtr = reinterpret_cast<char*>(intPtr);
            charPtr -= sentinelVal;
            intPtr = reinterpret_cast<int*>(charPtr);
            *(--intPtr) = sentinelVal;          // write left sentinel
        }

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     *
     * Calls the deconstructor for the T object at pointer p
     * @param p the pointer containing the T object to destroy
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     *
     * The getitem operator returns the data at index i as an int reference
     * @param i the index into the array
     * @return an int reference to the requested item
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     *
     * The const getitem operator returns the data at index i as a const int reference
     * @param i the index into the array
     * @return a const int reference to the requested item
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     *
     * Gets an iterator to the first block of data
     * @return return an iterator at the starting sentinel of the first block
     */
    iterator begin () {
        // return iterator(&(*this)[0]);
        int* ptr = reinterpret_cast<int*>(a);
        return iterator(ptr);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     *
     * Gets a const_iterator to the first block of data
     * @return return a const_iterator at the starting sentinel of the first block
     */
    const_iterator begin () const {
        // return const_iterator(const_cast<int*>(&(*this)[0]));
        const int* ptr = reinterpret_cast<const int*>(a);
        int* ptr2 = const_cast<int*>(ptr);
        return const_iterator(ptr2);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     *
     * Gets an iterator to the last block of data
     * @return return an iterator at the starting sentinel of the last block
     */
    iterator end () {
        char* cptr = &a[0];
        cptr = cptr + (N - sizeof(int));
        int* iptr = reinterpret_cast<int*>(cptr);
        int sentVal = *iptr;
        sentVal = abs(sentVal);
        cptr = reinterpret_cast<char*>(iptr);
        cptr = cptr - sentVal - sizeof(int);
        int* ptr = reinterpret_cast<int*>(cptr);
        return iterator(ptr);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     *
     * Gets a const iterator to the last block of data
     * @return return a const_iterator at the starting sentinel of the last block
     */
    const_iterator end () const {
        const char* cptr = &a[0];
        char* cptr2 = const_cast<char*>(cptr);
        cptr2 = cptr2 + (N - sizeof(int));
        int* iptr = reinterpret_cast<int*>(cptr2);
        int sentVal = *iptr;
        sentVal = abs(sentVal);
        cptr2 = reinterpret_cast<char*>(iptr);
        cptr2 = cptr2 - sentVal - sizeof(int);
        int* ptr = reinterpret_cast<int*>(cptr2);
        return const_iterator(ptr);
    }
};


#endif // Allocator_h
