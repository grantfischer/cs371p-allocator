// ---------------------
// RunAllocator.c++
// @author Grant Fischer
// Date: 03/30/2020
// ---------------------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // cin, cout
#include <vector>   // vector
#include <string>   // string, stoi()

#include "Allocator.hpp"

using namespace std;


// ---------
// constants
// ---------

const size_t c_heapSize = 1000;     // in bytes
const size_t c_deallocAmt = 1;

// -------------
// allocate_read
// -------------

istream& allocate_read (istream& r, vector<int>& blocks) {
    string num;
    while (getline(r, num)) {
        if (num == "" || num == "\n" || num == "\r")
            break;
        blocks.push_back((size_t) stoi(num));
    }
    return r;
}

// -------------
// allocate_eval
// -------------

void allocate_eval (vector<int>& blocks, vector<int>& resulting_sentinels) {
    // perform operations
    my_allocator<size_t, c_heapSize> myalloc;
    for (size_t i = 0; i < blocks.size(); i++) {
        assert(blocks[i] != 0);
        if (blocks[i] > 0) {
            myalloc.allocate(blocks[i]);
        }
        else {
            int block = blocks[i] * -1;
            int temp = 0;
            my_allocator<size_t, c_heapSize>::iterator b = begin(myalloc);
            while (temp != block) {
                if (*b < 0)
                    ++temp;
                if (temp == block)
                    break;
                ++b;
            }
            int* intPtr = &*b;
            ++intPtr;
            size_t* ptr = reinterpret_cast<size_t*>(intPtr);
            myalloc.deallocate(ptr, 1);
        }
    }

    // get results
    my_allocator<size_t, c_heapSize>::iterator b = begin(myalloc);
    my_allocator<size_t, c_heapSize>::iterator e = end(myalloc);
    while (b != e) {
        resulting_sentinels.push_back(*b);
        ++b;
    }
    resulting_sentinels.push_back(*e);
}

// --------------
// allocate_print
// --------------

void allocate_print (ostream& w, vector<int>& resulting_sentinels) {
    for (size_t i = 0; i < resulting_sentinels.size(); ++i) {
        if (i != resulting_sentinels.size() - 1)
            w << resulting_sentinels[i] << " ";
        else
            w << resulting_sentinels[i];
    }
    w << endl;
}

// ----
// main
// ----

int main () {
    /*
    your code for the real eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */

    int numTestCases;
    cin >> numTestCases;

    string temp;
    getline(cin, temp);
    getline(cin, temp);

    assert(numTestCases > 0);
    assert(numTestCases <= 100);

    //REPL
    for (int i = 0; i < numTestCases; ++i) {
        vector<int> blocks;
        allocate_read(cin, blocks);
        vector<int> resulting_sentinels;
        allocate_eval(blocks, resulting_sentinels);
        allocate_print(cout, resulting_sentinels);
    }

    return 0;
}
