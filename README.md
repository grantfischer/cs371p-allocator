# CS371p: Object-Oriented Programming Allocator Repo

* Name: Grant Fischer

* EID: gef432

* GitLab ID: grantfischer

* HackerRank ID: fishbros

* Git SHA: ce0ea163b879826e5d754ce5ba9fa3cc441a25fb

* GitLab Pipelines: https://gitlab.com/grantfischer/cs371p-allocator/pipelines

* Estimated completion time: 20

* Actual completion time: 40

* Comments: N/A
